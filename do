#!/bin/bash

source config

function realport {
  if [ $1 = "pp" ] || [ $1 = "bp" ] || [ $1 = "tp" ];then
    true
  else
    false
  fi
}

function find_replace {
    #manually use the fine replace syntax with ' instead of " surrounding s****g if the " character is involved. Using ' means that variables (as well as ', obviously) can't be used in search/replace, which is why it's not being used in this function.
    find $1 -type f -exec perl -pi -e "s[$2]($3)g" {} \;
}

function configured {
  if [ $configuration_set = "config_is_set" ];then
    true
  else
    echo "'./do configure' has not been ran. Please run this to setup initial configuration before proceeding."
    false
  fi
}

function not_configured {
  if [ $configuration_set = "config_not_set" ];then
    true
  else
    echo "'./do configure' has already been ran. Edit config file manually"
    false
  fi
}

function recompile {
  find_replace source/server.cpp blahblahblahblah $mysql_passwd
  find_replace source/server.cpp mud_home_directory $home_directory
  find_replace source/Makefile mud_home_directory $home_directory
  sudo mkdir source/bin
  target_port_number=$1"_port"
  # I.e. if this=pp_port, given pp_port=4500, so does this=${!this}
  target_port_number=${!target_port_number}
  #See local configuration settings for *p and *p_port values
  target_port=$1 
  target_port=${target_port}
  echo "Compiling for $1, then resetting files back to defaults"
  find_replace source/server.cpp configured_in_do target_port
  find_replace source/server.cpp 4446 target_port_number
  sudo make -C source #changes to source and then runs make
  find_replace source/server.cpp target_port configured_in_do 
  find_replace source/server.cpp target_port_number 4446 
  find_replace source/server.cpp $mysql_passwd blahblahblahblah
  find_replace source/server.cpp $home_directory mud_home_directory
  find_replace source/Makefile $home_directory mud_home_directory
  sudo find source -type f -name '*.o' -exec rm {} +
  sudo mv -f source/bin $1
  sudo mv -f source/temp $1
  sudo mv -f source/crashes $1
  sudo chmod 771 $1/bin/
  sudo chmod 771 $1/bin/server
  sudo chown root:root $1/bin
  sudo chown root:root $1/bin/server
}

function start {
  printf "\n Starting server in $1\n\n"
  cd $1
  sudo nohup ./start-server $1 &
}

function migrate {
  if [ $1 = "regions" ] || [ $1 = "package" ];then
    cp -rf bp/regions/. $2/regions
  fi
  
  if [ $1 = "source" ] || [ $1 = "package" ];then
    sudo mkdir $2/src
    cp -rf source/*.cpp $2/src
    cp -rf source/*.h $2/src
    cp -rf source/*.cc $2/src
    cp -rf source/*.rb $2/src
    cp -rf source/Makefile $2/src
  fi
}

function init {
  sudo mkdir $1 && sudo mkdir $1/bin && sudo mkdir $1/crashes && sudo mkdir $1/tmp
  sudo cp -rf source/generic/ $1
  sudo cp -rf source/lib/ $1
  sudo cp -rf source/regions $1
  sudo cp -f source/start-server $1
  echo "Configuring server to compile under given port"
  recompile $1
  sudo chmod 755 $1/start-server
  sudo chown root:root $1/start-server
}


#One, two, and three can have their values set by a different file (do_what) for the purpose of remotely queing commands. So it's important that the scripts receive their work the same way to reduce unneeded duplication.
one=$1
two=$2
three=$3

# This checks do_what for remote commands. Do_daemon (if running) checks do_what for changes and calls this file with the argument wake if so.
if [ "$1" = "wake" ];then
  wake="1"
  source do_what
  if [ $work = "yes" ];then
 # This gives a chance for any recent edits to hit before doing work.
    sleep 5
    find do_what -type f -exec perl -pi -e 's/work="yes"/work="no"/g' {} \;
  else
    one="file_change_detected"
    two="but"
    three="work_not_yes"
  fi
fi


if [ "$one" = "configure" ] && not_configured;then
    find_replace config config_not_set config_is_set
    printf "\nWhat's your mysql admin name going to be? I.e. rpiadmin:"
    read response
    find_replace config rpiadmin $response
    printf "\nTime to choose a passsword for your mysql rpiadmin. Don't choose what you're using for root/sudo. Make note of this password - you'll need it later on. In this config, no [ ( ' or \" allowed:"
    read response 
    find_replace config blahblahblahblah $response
    current_directory=${PWD##}
    printf "\n Setting mud home directory based on current direct."
    find_replace config mud_home_directory $current_directory
    echo "You're ready for './do install'"
    quiet="1"
fi
 
if [ "$one" = "daemons" ];then
  ufw reload
  nohup ./do_daemon
  printf "\nWork que watch daemon started\n"
  quiet="1"
fi

if [ "$one" = "essentials" ]; then
  echo "First time installation initiated."
  echo "Running apt-get update."
  sudo apt-get update
  echo "Running apt-get upgrade"
  sudo apt-get upgrade
  #echo "Making rpiadmin ssh friendly with cloud9"
  #sudo mkdir .ssh
  #sudo chmod 700 .ssh
  #sudo touch .ssh/authorized_keys
  #sudo chmod 600 .ssh/authorized_keys
  echo "Copying some stuff. For science"
  cp source/config .
  echo "Setting a basic firewall. Opening a few ports, closing most."
  sudo ufw enable
  sudo ufw allow 22/tcp #ssh
  sudo ufw allow 80/tcp #Koding port, other
  sudo ufw allow 443/tcp
  sudo ufw allow 25/tcp
  sudo ufw allow 4500/tcp #player port
  sudo ufw allow 4501/tcp #building port
  sudo ufw allow 4502/tcp #testing port
  sudo ufw allow 56789/tcp #Koding port
  sudo ufw reload
  
  #echo "disabling sshing into root"
  #sudo find /etc/ssh/sshd_config -type f -exec perl -pi -e "s/PermitRootLogin yes/PermitRootLogin no/g" {} \;
  quiet="1"
  printf "Run './do configure' next!"
fi

if [ "$one" = "install" ] && configured;then
printf "Are you sure you want to begin first time installation? Any current installation - pp, bp, tp - will be harmed! y/n:"
  read response
  if [ "$response" = "y" ]; then
    #echo "Creating arpi group, adding rpiadmin to group"
    #sudo groupadd arpi && sudo usermod -a -G arpi rpiadmin
    echo "Fetching dependencies:"
    source source/dependencies
    printf "\nYou are about to be asked for a MySQL password. Make note of it.\n"
    printf "\nSecuring MySQL installation. You may select yes for everything except changing the root password\n"
    quoted_mysqlhost="'"$mysql_host"'" #Need ' around this stuff
    quoted_passwd="'"$mysql_passwd"'" #because mysql
    sudo mysql_secure_installation
    echo "Opening MySQL as root to create new user rpiadmin in MySQL with the password of $mysql_passwd and to create databases. You'll need to enter your root mysql password to do this below."
    sudo mysql -u root -p -e "CREATE USER $quoted_mysqlhost@'localhost' IDENTIFIED BY $quoted_passwd; CREATE DATABASE rpi_engine; CREATE DATABASE rpi_player; CREATE DATABASE rpi_player_log; CREATE DATABASE rpi_world_log; GRANT ALL PRIVILEGES ON rpi_engine.* to $quoted_mysqlhost@'localhost'; GRANT ALL PRIVILEGES ON rpi_player.* to $quoted_mysqlhost@'localhost'; GRANT ALL PRIVILEGES ON rpi_player_log.* to $quoted_mysqlhost@'localhost'; GRANT ALL PRIVILEGES ON rpi_world_log.* to $quoted_mysqlhost@'localhost';"
    printf "/nAdding user rpiadmin to system now. Note the password you chose. You can safely just skip questions about name/address/etc in a couple of seconds\n"
    echo "Adding SQL files to databases."
    cd source/generic/sql
    mysql_user_password_host="sudo mysql -u $mysql_host -p$mysql_passwd -h localhost"
    $mysql_user_password_host rpi_engine < rpi_engine.sql && $mysql_user_password_host rpi_player_log < rpi_player_log.sql && $mysql_user_password_host rpi_player < rpi_player.sql && $mysql_user_password_host rpi_world_log < rpi_world_log.sql && $mysql_user_password_host rpi_player < rpi_player_test_account.sql && $mysql_user_password_host rpi_engine < rpi_engine_test_account.sql
    cd ..;cd ..;cd ..
    quiet="1";superquiet="1"
    printf "\nAll done. Review your terminal to make sure you're error free.\n"
    echo "Run ./do all by itself to see a list of other server management scripts"
    echo "Run 'sudo ./do init tp'   to generate the tp port "
  fi
  quiet="1"
fi

if [[ "$one" == "recompile"* ]];then
  if [[ "$two" == *"p" ]];then
    if [ "$one" = "recompile" ];then
      recompile $two
    fi
  if [ "$one" = "recompiles" ];then
    recompile $two
    quiet="1"
    start $two
  fi
fi
fi

if [ "$one" = "init" ] || [ "$one" = "inits" ] && realport "$two" && configured;then
  if [ $one = "init" ];then
    init $two
  fi
  if [ $one = "inits" ];then
    init $two
    start $two
  fi
  quiet="1"
fi

if [[ "$one" == "migrate"* ]] && [ "$two" = "source" ] || [ "$two" = "regions" ] || [ "$two" = "package" ] && realport $three;then
  if [ $one = "migrate" ];then
    migrate $two $three
  fi
  if [ $one = "migrates" ] && [ $two = "regions" ];then
    migrate $two $three
    start $three
  fi
  if [ $one = "migratem" ];then
    migrate $two $three
    recompile $three
  fi
  if [ $one = "migratems" ];then
    migrate $two $three
    recompile $three
    start $three
  fi
  quiet="1"
fi

if [ "$one" = "backup" ] &&  [[ ! -z $2 ]];then
  directory=$two"_bak"
  cp -rf $two/. $directory
  quiet="1"
fi

if [ "$one" = "restore" ] &&  [[ ! -z $2 ]];then
  directory=$two"_bak"
  cp -rf $directory/. $two/.
  quiet="1"
fi


if [ "$wake" = "1" ];then
  tstamp=$(date '+%m-%d-%y %H:%M:%S;')
  find do_what -type f -exec perl -pi -e "s/#Jobs sent, most recent first:/#Jobs sent, most recent first:\n\n#$tstamp\n#$one $two $three/g" {} \;
fi

if [ "$one" = "start" ] && realport "$two";then
  start $two
fi

if [ "$quiet" != "1" ] && [[ -z $1 ]];then
  printf "\nThis file is used for first time configuration of OpenRPI (install) as well as managing the ports."
  echo " The syntax is ./do <arg1> <arg2>"
  echo "Valid arguments are:"
  printf "\ninstall\n"
  echo "start pp/bp/tp"
  echo "migrate source/regions/package (to) pp/bp/tp"
  echo "migrates regions (to) pp/bp/tp              --migrate regions and start server"
  echo "migratem source/regions/package (to) pp/bp/tp   --migrate and makes"
  echo "migratems source/regions/package (to) pp/bp/tp   --migrate, make, and start"
  echo "recompile pp/bp/tp    --recompile a port"
  echo "recompiles pp/bp/tp    --recompile and then start"
  echo "backup <directory>   --creates _bak folder"
  echo "restore <directory>   --restores from _bak"
  echo "init pp/bp/tp   --sets port up from source/materials"
  echo "inits pp/bp/tp           --sets port up from source/materials and starts it"
  superquiet="1"
fi

if [ "$superquiet" != "1" ];then
  printf "\n FYI: Run ./do all by itself to see a list of all arguments.\n"
fi
